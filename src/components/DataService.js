import { BehaviorSubject } from "rxjs";

function filter(fn) {
  console.log('fn', fn)
  this.source$.next(fn(this.source$.value));
}

function unfilter() {
  this.source$.next(this._original$.value);
}

function set(data) {
  this.source$.next(data);
  this._original$.next(data);
}

export const DataService = {
  source$: new BehaviorSubject([]),
  _original$: new BehaviorSubject([]),
  filter,
  unfilter,
  set,
};
